Fork del Driver portado a Java compatible con la familia de chipsets RTL8192, desarrollado por Milen Rangelov (https://github.com/gat3way/AirPirate/blob/master/src/com/gat3way/airpirate/Rtl8192Card.java).

Se ha modificado para que admita 60 de los 120 dispositivos comerciales enumerados por WikiDevi 

Este programa es software libre; puede ser redistribuído y/o modificado bajo los términos de GNU GPL v2 tal como publica la Fundación de Software Libre.